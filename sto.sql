/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.6.16 : Database - support_ticket
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `report_bug` */

CREATE TABLE `report_bug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) DEFAULT NULL,
  `report_bug` text,
  `create_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `answer` text,
  `statusbug` enum('pending','confirmed','fix') DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `report_bug` */

insert  into `report_bug`(`id`,`menu`,`report_bug`,`create_date`,`user_id`,`answer`,`statusbug`) values (30,'dsad','asdada','2015-03-05 06:06:00',23546,NULL,'pending');

/*Table structure for table `ticket_answer` */

CREATE TABLE `ticket_answer` (
  `uuid` varchar(36) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `user_uuid` varchar(36) DEFAULT NULL,
  `answer` text,
  `file_uuid` varchar(36) DEFAULT NULL,
  `ticket_uuid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_answer` */

/*Table structure for table `ticket_auth_user` */

CREATE TABLE `ticket_auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `modul_id` int(11) DEFAULT NULL,
  `create` enum('n','y') DEFAULT 'n',
  `update` enum('n','y') DEFAULT 'n',
  `delete` enum('n','y') DEFAULT 'n',
  `print` enum('n','y') DEFAULT 'n',
  `view` enum('n','y') DEFAULT 'n',
  PRIMARY KEY (`id`),
  KEY `modul_id` (`modul_id`),
  KEY `user_typeid` (`type_id`),
  CONSTRAINT `ticket_auth_user_ibfk_1` FOREIGN KEY (`modul_id`) REFERENCES `ticket_modul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_auth_user` */

insert  into `ticket_auth_user`(`id`,`type_id`,`modul_id`,`create`,`update`,`delete`,`print`,`view`) values (1,0,1,'y','y','y','y','y'),(2,0,2,'y','y','y','y','y'),(3,0,3,'y','y','y','y','y'),(4,0,4,'y','y','y','y','y'),(5,0,5,'y','y','y','y','y'),(6,0,6,'y','y','y','y','y'),(7,0,7,'y','y','y','y','y'),(8,0,8,'y','y','y','y','y'),(10,0,10,'y','y','y','y','y'),(11,0,11,'y','y','y','y','y'),(12,0,12,'y','y','y','y','y'),(13,0,14,'y','y','y','y','y'),(14,0,15,'y','y','y','y','y'),(15,0,16,'y','y','y','y','y'),(16,0,17,'y','y','y','y','y'),(17,0,18,'y','y','y','y','y'),(18,0,19,'y','y','y','y','y'),(19,1,12,'y','y','y','y','y'),(20,0,20,'y','y','y','y','y'),(21,0,22,'y','y','y','y','y'),(22,0,21,'y','y','y','y','y'),(23,0,23,'y','y','y','y','y'),(24,0,24,'y','y','y','y','y');

/*Table structure for table `ticket_ci_sessions` */

CREATE TABLE `ticket_ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_ci_sessions` */

insert  into `ticket_ci_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('003cd2c2007ceb0259a8ac41ca20c947','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',1426288283,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:22:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"template_answer\";a:6:{s:3:\"url\";s:15:\"template_answer\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"register_member\";a:6:{s:3:\"url\";s:15:\"register_member\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"list_ticket\";a:6:{s:3:\"url\";s:11:\"list_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"assign_ticket\";a:6:{s:3:\"url\";s:13:\"assign_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"replay_ticket\";a:6:{s:3:\"url\";s:13:\"replay_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"knowledgebases\";a:6:{s:3:\"url\";s:14:\"knowledgebases\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('06d72fdea95cbc3365ff62e3b44dc195','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2323.2 Safari/537.36',1425679418,''),('09632397b2aadf5f742a2b319bc8520c','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2325.0 Safari/537.36',1425887957,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:21:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"template_answer\";a:6:{s:3:\"url\";s:15:\"template_answer\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"register_member\";a:6:{s:3:\"url\";s:15:\"register_member\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"list_ticket\";a:6:{s:3:\"url\";s:11:\"list_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"assign_ticket\";a:6:{s:3:\"url\";s:13:\"assign_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"replay_ticket\";a:6:{s:3:\"url\";s:13:\"replay_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('191e5e35bf46e9f5a19ab01adb47d710','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2331.1 Safari/537.36',1426290155,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:22:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"template_answer\";a:6:{s:3:\"url\";s:15:\"template_answer\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"register_member\";a:6:{s:3:\"url\";s:15:\"register_member\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"list_ticket\";a:6:{s:3:\"url\";s:11:\"list_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"assign_ticket\";a:6:{s:3:\"url\";s:13:\"assign_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"replay_ticket\";a:6:{s:3:\"url\";s:13:\"replay_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"knowledgebases\";a:6:{s:3:\"url\";s:14:\"knowledgebases\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('1be1e0522b94880858ecd6676bcce29d','192.168.1.1','0',1426123902,''),('2759fbbd80c904fcdc4f08a53ab6da53','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2328.0 Safari/537.36',1426116121,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:21:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"template_answer\";a:6:{s:3:\"url\";s:15:\"template_answer\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"register_member\";a:6:{s:3:\"url\";s:15:\"register_member\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"list_ticket\";a:6:{s:3:\"url\";s:11:\"list_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"assign_ticket\";a:6:{s:3:\"url\";s:13:\"assign_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"replay_ticket\";a:6:{s:3:\"url\";s:13:\"replay_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('355d2c7cbf22774e63a597ae82a359e7','192.168.1.1','0',1426059476,''),('4c8b5771ad917d03b95c20de0872f524','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2327.5 Safari/537.36',1426031264,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:36:\"8ae12f05-46d3-4b58-b94d-68bd8b819f94\";s:4:\"name\";s:6:\"ardani\";s:4:\"tipe\";s:1:\"1\";s:5:\"menus\";a:1:{s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('4ff05ce4dd76649a449745bb3031fb4e','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2331.1 Safari/537.36',1426286546,''),('533ed0f1d8b782c92fdac063ed980c5e','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2331.1 Safari/537.36',1426286531,''),('59bf8c8365c3a14daf2490a434859ef1','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:38.0) Gecko/20100101 Firefox/38.0',1425871658,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:17:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"template_answer\";a:6:{s:3:\"url\";s:15:\"template_answer\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('6c739ba82ef3bc7de41e724e95644848','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2321.0 Safari/537.36',1425512604,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:17:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"template\";a:6:{s:3:\"url\";s:8:\"template\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('70ccfac4bad5c900587da4420375782f','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2323.0 Safari/537.36',1425597934,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:17:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"template\";a:6:{s:3:\"url\";s:8:\"template\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('84af73cac598fe809cb2c39b89cf6560','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2331.1 Safari/537.36',1426286582,'a:4:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";}'),('9201b65da582462966218bec941ad3b3','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2323.2 Safari/537.36',1425684113,'a:5:{s:9:\"user_data\";s:0:\"\";s:6:\"userid\";s:5:\"23546\";s:4:\"name\";s:15:\"admin is greate\";s:4:\"tipe\";s:1:\"0\";s:5:\"menus\";a:17:{s:8:\"employee\";a:6:{s:3:\"url\";s:8:\"employee\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"departement\";a:6:{s:3:\"url\";s:11:\"departement\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:15:\"template_answer\";a:6:{s:3:\"url\";s:15:\"template_answer\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"create_ticket\";a:6:{s:3:\"url\";s:13:\"create_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"ticket_status\";a:6:{s:3:\"url\";s:13:\"ticket_status\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"ticket_topic\";a:6:{s:3:\"url\";s:12:\"ticket_topic\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"find_ticket\";a:6:{s:3:\"url\";s:11:\"find_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:8:\"priority\";a:6:{s:3:\"url\";s:8:\"priority\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:12:\"upload_modul\";a:6:{s:3:\"url\";s:12:\"upload_modul\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"knowledgebase\";a:6:{s:3:\"url\";s:13:\"knowledgebase\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"modul_cat\";a:6:{s:3:\"url\";s:9:\"modul_cat\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:14:\"setting_option\";a:6:{s:3:\"url\";s:14:\"setting_option\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:13:\"report_ticket\";a:6:{s:3:\"url\";s:13:\"report_ticket\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:10:\"report_bug\";a:6:{s:3:\"url\";s:10:\"report_bug\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:16:\"authority_access\";a:6:{s:3:\"url\";s:16:\"authority_access\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:9:\"type_user\";a:6:{s:3:\"url\";s:9:\"type_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}s:11:\"access_user\";a:6:{s:3:\"url\";s:11:\"access_user\";s:4:\"view\";s:1:\"y\";s:6:\"create\";s:1:\"y\";s:6:\"update\";s:1:\"y\";s:6:\"delete\";s:1:\"y\";s:5:\"print\";s:1:\"y\";}}}'),('97353f93456ff73648e25a81969ce839','192.168.1.1','0',1426036538,''),('eaf657445ef7b828a560c4c83d903a97','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2327.5 Safari/537.36',1426026062,'');

/*Table structure for table `ticket_departement` */

CREATE TABLE `ticket_departement` (
  `departement_id` int(11) NOT NULL AUTO_INCREMENT,
  `departement_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`departement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_departement` */

insert  into `ticket_departement`(`departement_id`,`departement_name`) values (1,'departement 1'),(2,'depart 2'),(4,'dasdasdad'),(5,'sadasdad');

/*Table structure for table `ticket_file` */

CREATE TABLE `ticket_file` (
  `uuid` varchar(36) NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `file_type` varchar(30) DEFAULT NULL,
  `file_size` varchar(30) DEFAULT NULL,
  `file_name_encrypt` varchar(100) DEFAULT NULL,
  `ticket_uuid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_file` */

insert  into `ticket_file`(`uuid`,`file_name`,`file_type`,`file_size`,`file_name_encrypt`,`ticket_uuid`) values ('c045d352-d78e-4c43-adb7-98ea27b9cff7','Attachment','.pdf','289.89','702a1d61bfdc8344d64e7413194b48a8.pdf','a66bb271-7c36-4d64-9af6-b66b343bbeba'),('c53a8ea2-e6f7-4b02-93ee-5440e7c604e3','Attachment','.jpg','46.89','44b002f7a30903145a754d540ad4249f.jpg','7bdd89db-401b-4eaa-b691-c70fad221c14'),('d016165d-88c8-41d6-8049-a238b9d1b5f8','Attachment','.pdf','289.89','1ee80cf9b61443cc5f5a1f9428791146.pdf','101684d3-5125-4255-a44f-60c34093eeaa');

/*Table structure for table `ticket_knowledgebase` */

CREATE TABLE `ticket_knowledgebase` (
  `uuid` varchar(36) NOT NULL,
  `content` text,
  `title` varchar(200) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT '0000-00-00',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_knowledgebase` */

insert  into `ticket_knowledgebase`(`uuid`,`content`,`title`,`created`,`updated`) values ('e179aaa9-361f-426e-a21c-410cbd637831','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Sdasdasda','2015-03-06','0000-00-00');

/*Table structure for table `ticket_log` */

CREATE TABLE `ticket_log` (
  `uuid` varchar(36) NOT NULL,
  `user_uuid` varchar(36) DEFAULT NULL,
  `log` text,
  `type` varchar(45) DEFAULT NULL,
  `ticket_uuid` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_log` */

/*Table structure for table `ticket_modul` */

CREATE TABLE `ticket_modul` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modul_name` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `modul_catid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modul_catid` (`modul_catid`),
  CONSTRAINT `ticket_modul_ibfk_1` FOREIGN KEY (`modul_catid`) REFERENCES `ticket_modul_cat` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_modul` */

insert  into `ticket_modul`(`id`,`modul_name`,`icon`,`url`,`modul_catid`) values (1,'Employee','moon-user-3','employee',1),(2,'Departement','moon-office ','departement',1),(3,'Authority Access','moon-equalizer ','authority_access',5),(4,'Upload Modul','moon-upload-3 ','upload_modul',3),(5,'Knonwledge Base','moon-bookmarks','knowledgebase',3),(6,'Modul Category','moon-fire ','modul_cat',3),(7,'User Type','moon-user-2','type_user',5),(8,'User Access','moon-lock','access_user',5),(10,'Create Ticket','moon-pencil','create_ticket',2),(11,'Status Ticket','moon-list','ticket_status',2),(12,'Ticket Topic','moon-star-3','ticket_topic',2),(14,'Template','moon-star-3','template_answer',1),(15,'Setting Option','moon-cart-3','setting_option',3),(16,'Find Ticket','moon-bookmarks','find_ticket',2),(17,'Report Ticket','moon-print','report_ticket',4),(18,'Report Bug','moon-quotes-left','report_bug',4),(19,'Priority Ticket','moon-fire','priority',2),(20,'Register Member','moon-user-2','register_member',1),(21,'List Ticket','moon-bookmarks','list_ticket',2),(22,'Assign Ticket','moon-equalizer','assign_ticket',2),(23,'Replay Ticket','moon-fire','replay_ticket',2),(24,'Knownledge','moon-bookmarks','knowledgebases',2);

/*Table structure for table `ticket_modul_cat` */

CREATE TABLE `ticket_modul_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modul_cat` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_modul_cat` */

insert  into `ticket_modul_cat`(`id`,`modul_cat`,`icon`) values (1,'Master','icon-hdd'),(2,'Ticket','icon-folder-open'),(3,'Setting','icon-wrench'),(4,'Report','icon-print'),(5,'Access','icon-user');

/*Table structure for table `ticket_option` */

CREATE TABLE `ticket_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(45) DEFAULT NULL,
  `value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_option` */

insert  into `ticket_option`(`id`,`key`,`value`) values (1,'office.name','nama office'),(2,'office.address','test'),(3,'office.telp','021'),(4,'office.email','email');

/*Table structure for table `ticket_priority` */

CREATE TABLE `ticket_priority` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_priority` */

insert  into `ticket_priority`(`priority_id`,`priority`) values (1,'normal'),(2,'urgent'),(3,'critical');

/*Table structure for table `ticket_status` */

CREATE TABLE `ticket_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_status` */

insert  into `ticket_status`(`status_id`,`status`) values (1,'open'),(2,'review'),(3,'solved'),(4,'close');

/*Table structure for table `ticket_template_answer` */

CREATE TABLE `ticket_template_answer` (
  `uuid` varchar(36) NOT NULL,
  `template` text,
  `title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_template_answer` */

insert  into `ticket_template_answer`(`uuid`,`template`,`title`) values ('d6660726-f9b5-44e2-ba09-1571e638c74b','terima kasih untuk pertanyaannya','template 1');

/*Table structure for table `ticket_ticket` */

CREATE TABLE `ticket_ticket` (
  `uuid` varchar(36) NOT NULL,
  `no_ticket` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_uuid` varchar(36) DEFAULT NULL,
  `departement_id` int(11) DEFAULT '0',
  `title` varchar(45) DEFAULT NULL,
  `question` text,
  `date` date DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  `topic_id` int(11) DEFAULT NULL,
  `update` timestamp NULL DEFAULT NULL,
  `closed` char(1) DEFAULT '0',
  `assign_uuid` varchar(36) DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `no_ticket` (`no_ticket`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_ticket` */

insert  into `ticket_ticket`(`uuid`,`no_ticket`,`user_uuid`,`departement_id`,`title`,`question`,`date`,`status_id`,`topic_id`,`update`,`closed`,`assign_uuid`,`priority_id`) values ('2f95c909-eb8e-463d-9324-df23ecede249',0003,'23546',0,'Mobile Support','testjkgkggk','2015-03-09',1,1,NULL,'0',NULL,1),('33923b6e-18d6-4b55-98b8-27fe9dd4eae2',0002,'23546',0,'Mobile Support','test','2015-03-09',1,1,NULL,'0',NULL,1),('4e4ec0de-52ac-4780-bb0b-9fa145ded522',0001,'23546',0,'JDV Theme','test','2015-03-09',1,1,NULL,'0',NULL,1),('7bdd89db-401b-4eaa-b691-c70fad221c14',0006,'23546',0,'inii uji coba file','contoh saja dari sample','2015-03-12',1,1,NULL,'0',NULL,1),('a66bb271-7c36-4d64-9af6-b66b343bbeba',0004,'23546',0,'Sdasda','asdasda','2015-03-12',1,1,NULL,'0',NULL,1),('b8dad45c-87d4-4896-970a-31b0e86fd200',0005,'23546',0,'sdasd','asdasdada','2015-03-12',1,1,NULL,'0',NULL,1);

/*Table structure for table `ticket_ticket_has_file` */

CREATE TABLE `ticket_ticket_has_file` (
  `ticket_uuid` varchar(36) DEFAULT NULL,
  `file_uuid` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_ticket_has_file` */

/*Table structure for table `ticket_topic` */

CREATE TABLE `ticket_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_topic` */

insert  into `ticket_topic`(`topic_id`,`topic`) values (1,'umum'),(2,'khusus'),(3,'tata cara');

/*Table structure for table `ticket_type_user` */

CREATE TABLE `ticket_type_user` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_type_user` */

insert  into `ticket_type_user`(`type_id`,`name`) values (0,'administrator'),(1,'member'),(2,'office'),(3,'operator'),(4,'dsdasdasdad');

/*Table structure for table `ticket_user` */

CREATE TABLE `ticket_user` (
  `uuid` varchar(36) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `actived` char(1) DEFAULT '0',
  `full_name` varchar(50) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `type_id` int(11) DEFAULT '1',
  `departement_id` int(11) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `identity_no` varchar(25) DEFAULT NULL,
  `type_identity` char(5) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `work` varchar(50) DEFAULT NULL,
  `rt` char(10) DEFAULT NULL,
  `rw` char(10) DEFAULT NULL,
  `village` varchar(50) DEFAULT NULL,
  `sub` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_user` */

insert  into `ticket_user`(`uuid`,`email`,`password`,`actived`,`full_name`,`phone`,`address`,`type_id`,`departement_id`,`username`,`avatar`,`identity_no`,`type_identity`,`gender`,`work`,`rt`,`rw`,`village`,`sub`,`province`,`last_login`) values ('23546','admin@admin.com','21232f297a57a5a743894a0e4a801fc3','1','admin is greate','021','yogyakarta',0,1,'admin',NULL,'1','1','L','pegawai','02','03','gondang','gondang','jawa tengah','2015-03-14 05:45:00'),('8ae12f05-46d3-4b58-b94d-68bd8b819f94','ardani@skyshi.com','2c28abddae416b5a25d6a89688f4f96e','1','ardani','085642201514','solo',1,NULL,NULL,NULL,'546','SIM','L',NULL,'7','5','solo','solo','solo',NULL),('aa697d14-b84f-4d06-8c04-695e269cccf8','master.ardani@gmail.com',NULL,'1','ardani','2356','solo',2,4,NULL,NULL,'2356844552','SIM','L',NULL,'7','5','siwal','baki','jawa tengah',NULL),('f6d10287-07c5-4f1f-b29e-c504e065256d','ardani@skyshi2.com','2c28abddae416b5a25d6a89688f4f96e','1','ardani','085642201514','solo',1,NULL,NULL,NULL,'546','SIM','L',NULL,'7','4','siwal','asdas','a',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
